#!/usr/bin/env python3
"""
Script that generates similarity search results for running tests
"""
import time
import argparse
import os
import json
import datetime

import yaml

import requests

PARSER = argparse.ArgumentParser()
PARSER.add_argument('context_type', help='Type of context to generate',
                    choices=['SIMILARITY', 'SUBSTRUCTURE', 'CONNECTIVITY', 'BLAST', 'SEARCH_BY_IDS',
                             'SEARCH_BY_IDS_SDF', 'DIRECT_SIMILARITY', 'DIRECT_SIMILARITY_SDF'])
ARGS = PARSER.parse_args()


class ContextGeneratorError(Exception):
    """
    Base class for exceptions in this module
    """


def run():
    """
    Runs the script
    """
    if ARGS.context_type == 'SIMILARITY':
        job_id = launch_and_wait_for_similarity_search_job()
        generate_job_params_file(job_id, 'SIMILARITY', 'run_params_example5_template.yml', 'run_params_example5.yml')
    elif ARGS.context_type == 'SUBSTRUCTURE':
        job_id = launch_and_wait_for_substructure_search_job()
        generate_job_params_file(job_id, 'SUBSTRUCTURE', 'run_params_example6_template.yml', 'run_params_example6.yml')
    elif ARGS.context_type == 'CONNECTIVITY':
        job_id = launch_and_wait_for_connectivity_search_job()
        generate_job_params_file(job_id, 'CONNECTIVITY', 'run_params_example7_template.yml', 'run_params_example7.yml')
    elif ARGS.context_type == 'BLAST':
        job_id = launch_and_wait_for_blast_search_job()
        generate_job_params_file(job_id, 'BLAST', 'run_params_example8_template.yml', 'run_params_example8.yml')
    elif ARGS.context_type == 'SEARCH_BY_IDS':
        job_id = launch_and_wait_for_search_by_ids_job()
        generate_job_params_file(job_id, 'TO_COMPOUNDS', 'run_params_example10_template.yml',
                                 'run_params_example10.yml')
    elif ARGS.context_type == 'SEARCH_BY_IDS_SDF':
        job_id = launch_and_wait_for_search_by_ids_job()
        generate_job_params_file(job_id, 'TO_COMPOUNDS', 'run_params_example11_template.yml',
                                 'run_params_example11.yml')
    elif ARGS.context_type == 'DIRECT_SIMILARITY':
        smiles = 'COc1cc2c(c(OC)c1OC)-c1ccc(Br)c(=O)cc1[C@@H](NC(C)=O)CC2'
        similarity_threshold = 70
        generate_job_params_file(smiles, 'DIRECT_SIMILARITY', 'run_params_example12_template.yml',
                                 'run_params_example12.yml', similarity_threshold)
    elif ARGS.context_type == 'DIRECT_SIMILARITY_SDF':
        smiles = 'COc1cc2c(c(OC)c1OC)-c1ccc(Br)c(=O)cc1[C@@H](NC(C)=O)CC2'
        similarity_threshold = 70
        generate_job_params_file(smiles, 'DIRECT_SIMILARITY', 'run_params_example13_template.yml',
                                 'run_params_example13.yml', similarity_threshold)


# ----------------------------------------------------------------------------------------------------------------------
# Search by IDs
# ----------------------------------------------------------------------------------------------------------------------
def launch_and_wait_for_search_by_ids_job():
    """
    launches and waits until a Search by IDS job finishes
    :return: the job id of the job
    """
    print('GENERATING SEARCH BY IDS RESULTS!')
    delayed_jobs_server_base_path = get_delayed_jobs_server_base_path()
    job_id = submit_search_by_ids_job(delayed_jobs_server_base_path)
    wait_util_job_finishes(delayed_jobs_server_base_path, job_id)

    return job_id


def submit_search_by_ids_job(delayed_jobs_server_base_path):
    """
    :param delayed_jobs_server_base_path: base path of the delayed jobs server
    :return: job_id
    """
    print('Launching a search by IDs job...')
    submission_url = get_url_for_search_by_ids_job_submission(delayed_jobs_server_base_path)
    print('submission_url: ', submission_url)

    payload = {
        'from': 'MOLECULE_CHEMBL_IDS',
        'to': 'CHEMBL_COMPOUNDS',
        'separator': ',',
        'raw_items_ids': 'CHEMBL27193,CHEMBL4068896,CHEMBL332148,CHEMBL2431212,CHEMBL4303667',
        'dl__ignore_cache': False
    }
    print('payload: ', payload)

    return submit_delayed_job(submission_url, payload)


# ----------------------------------------------------------------------------------------------------------------------
# BLAST Search
# ----------------------------------------------------------------------------------------------------------------------
def launch_and_wait_for_blast_search_job():
    """
    launches and waits until a BLAST search job finishes
    :return: the job id of the job
    """
    print('GENERATING BLAST RESULTS!')
    delayed_jobs_server_base_path = get_delayed_jobs_server_base_path()
    job_id = submit_blast_search_job(delayed_jobs_server_base_path)
    wait_util_job_finishes(delayed_jobs_server_base_path, job_id)

    return job_id


def submit_blast_search_job(delayed_jobs_server_base_path):
    """
    :param delayed_jobs_server_base_path: base path of the delayed jobs server
    :return: job_id
    """
    print('Launching a substructure search job...')
    submission_url = get_url_for_blast_job_submission(delayed_jobs_server_base_path)
    print('submission_url: ', submission_url)

    payload = {
        'sequence': '>sp|P35858|ALS_HUMAN Insulin-like growth factor-binding protein complex acid labile subunit '
                    'OS=Homo sapiens GN=IGFALS PE=1 SV=1 \nMALRKGGLALALLLLSWVALGPRSLEGADPGTPGEAEGPACPAACVCSYDDDADELSVFC'
                    '\nSSRNLTRLPDGVPGGTQALWLDGNNLSSVPPAAFQNLSSLGFLNLQGGQLGSLEPQALLG\nLENLCHLHLERNQLRSLALGTFAHTPALASLGLS'
                    'NNRLSRLEDGLFEGLGSLWDLNLGWN\nSLAVLPDAAFRGLGSLRELVLAGNRLAYLQPALFSGLAELRELDLSRNALRAIKANVFVQ\nLPRLQKLY'
                    'LDRNLIAAVAPGAFLGLKALRWLDLSHNRVAGLLEDTFPGLLGLRVLRLSHN\nAIASLRPRTFKDLHFLEELQLGHNRIRQLAERSFEGLGQLEVLT'
                    'LDHNQLQEVKAGAFLG\nLTNVAVMNLSGNCLRNLPEQVFRGLGKLHSLHLEGSCLGRIRPHTFTGLSGLRRLFLKDN\nGLVGIEEQSLWGLAELLE'
                    'LDLTSNQLTHLPHRLFQGLGKLEYLLLSRNRLAELPADALGP\nLQRAFWLDVSHNRLEALPNSLLAPLGRLRYLSLRNNSLRTFTPQPPGLERLWLE'
                    'GNPWDC\nGCPLKALRDFALQNPSAVPRFVQAICEGDDCQPPAYTYNNITCASPPEVVGLDLRDLSEA\nHFAPC',
        'dl__ignore_cache': True
    }
    print('payload: ', payload)

    return submit_delayed_job(submission_url, payload)


# ----------------------------------------------------------------------------------------------------------------------
# Connectivity Search
# ----------------------------------------------------------------------------------------------------------------------

def launch_and_wait_for_connectivity_search_job():
    """
    launches and waits until a substructure search job finishes
    :return: the job id of the job
    """
    print('GENERATING CONNECTIVITY RESULTS!')
    delayed_jobs_server_base_path = get_delayed_jobs_server_base_path()
    job_id = submit_connectivity_search_job(delayed_jobs_server_base_path)
    wait_util_job_finishes(delayed_jobs_server_base_path, job_id)

    return job_id


def submit_connectivity_search_job(delayed_jobs_server_base_path):
    """
    :param delayed_jobs_server_base_path: base path of the delayed jobs server
    :return: job_id
    """
    print('Launching a substructure search job...')
    submission_url = get_url_for_similarity_job_submission(delayed_jobs_server_base_path)
    print('submission_url: ', submission_url)

    payload = {
        'search_type': 'CONNECTIVITY',
        'search_term': 'CC1(C)[C@H]2CC[C@@]1(C)[C@H](O)[C@@H]2O',
        'dl__ignore_cache': False
    }
    print('payload: ', payload)

    return submit_delayed_job(submission_url, payload)


# ----------------------------------------------------------------------------------------------------------------------
# Substructure Search
# ----------------------------------------------------------------------------------------------------------------------
def launch_and_wait_for_substructure_search_job():
    """
    launches and waits until a substructure search job finishes
    :return: the job id of the job
    """
    print('GENERATING SUBSTRUCTURE RESULTS!')
    delayed_jobs_server_base_path = get_delayed_jobs_server_base_path()
    job_id = submit_substructure_search_job(delayed_jobs_server_base_path)
    wait_util_job_finishes(delayed_jobs_server_base_path, job_id)

    return job_id


def submit_substructure_search_job(delayed_jobs_server_base_path):
    """
    :param delayed_jobs_server_base_path: base path of the delayed jobs server
    :return: job_id
    """
    print('Launching a substructure search job...')
    submission_url = get_url_for_similarity_job_submission(delayed_jobs_server_base_path)
    print('submission_url: ', submission_url)

    payload = {
        'search_type': 'SUBSTRUCTURE',
        'search_term': 'CC1(C)[C@H]2CC[C@@]1(C)[C@H](O)[C@@H]2O',
        'dl__ignore_cache': False
    }
    print('payload: ', payload)

    return submit_delayed_job(submission_url, payload)


# ----------------------------------------------------------------------------------------------------------------------
# Similarity Search
# ----------------------------------------------------------------------------------------------------------------------
def launch_and_wait_for_similarity_search_job():
    """
    launches and waits until a similarity search job finishes
    :return: the job id of the job
    """
    print('GENERATING SIMILARITY RESULTS!')
    delayed_jobs_server_base_path = get_delayed_jobs_server_base_path()
    job_id = submit_similarity_search_job(delayed_jobs_server_base_path)
    wait_util_job_finishes(delayed_jobs_server_base_path, job_id)

    return job_id


def submit_similarity_search_job(delayed_jobs_server_base_path):
    """
    :param delayed_jobs_server_base_path: base path of the delayed jobs server
    :return: job_id
    """
    print('Launching a similarity search job...')
    submission_url = get_url_for_similarity_job_submission(delayed_jobs_server_base_path)
    print('submission_url: ', submission_url)

    payload = {
        'search_type': 'SIMILARITY',
        'search_term': 'CN1C[C@H](C(=O)N[C@]2(C)O[C@@]3(O)[C@@H]4CCCN4C(=O)[C@H](Cc4ccccc4)N3C2=O)CC2c3cccc4[nH]cc(c34)'
                       'C[C@H]21.CN1C[C@H](C(=O)N[C@]2(C)O[C@@]3(O)[C@@H]4CCCN4C(=O)[C@H](Cc4ccccc4)N3C2=O)C[C@@H]2c3cc'
                       'cc4[nH]cc(c34)C[C@H]21.O=C(O)C(O)C(O)C(=O)O',
        'threshold': 40,
        'dl__ignore_cache': False
    }
    print('payload: ', payload)

    return submit_delayed_job(submission_url, payload)


# ----------------------------------------------------------------------------------------------------------------------
# Similarity Search
# ----------------------------------------------------------------------------------------------------------------------
def get_url_for_job_status(delayed_jobs_base_url, job_id):
    """
    :param delayed_jobs_base_url: base url for the delayed jobs.
    E.g. https://www.ebi.ac.uk/chembl/interface_api/delayed_jobs
    :param job_id: job_id
    :return: url for getting a job status
    """
    return f'{delayed_jobs_base_url}/status/{job_id}'


def get_url_for_similarity_job_submission(delayed_jobs_base_url):
    """
    :param delayed_jobs_base_url: base url for the delayed jobs.
    E.g. https://www.ebi.ac.uk/chembl/interface_api/delayed_jobs
    :return: url for submitting a similarity search job
    """
    return f'{delayed_jobs_base_url}/submit/structure_search_job'


def get_url_for_blast_job_submission(delayed_jobs_base_url):
    """
    :param delayed_jobs_base_url: base url for the delayed jobs.
    E.g. https://www.ebi.ac.uk/chembl/interface_api/delayed_jobs
    :return: url for submitting a similarity search job
    """
    return f'{delayed_jobs_base_url}/submit/biological_sequence_search_job'


def get_url_for_search_by_ids_job_submission(delayed_jobs_base_url):
    """
    :param delayed_jobs_base_url: base url for the delayed jobs.
    E.g. https://www.ebi.ac.uk/chembl/interface_api/delayed_jobs
    :return: url for submitting a search by ids job
    """
    return f'{delayed_jobs_base_url}/submit/search_by_ids_job'


def get_delayed_jobs_server_base_path():
    """
    :return: the delayed jobs server base path
    """
    delayed_jobs_base_url = os.environ.get('DELAYED_JOBS_BASE_URL')

    if delayed_jobs_base_url is None:
        raise ContextGeneratorError(f'You need to set the DELAYED_JOBS_BASE_URL env variable!')

    return delayed_jobs_base_url


def get_es_proxy_server_base_path():
    """
    :return: the delayed jobs server base path
    """
    es_proxy_base_url = os.environ.get('ES_PROXY_BASE_URL')

    if es_proxy_base_url is None:
        raise ContextGeneratorError(f'You need to set the ES_PROXY_BASE_URL env variable!')

    return es_proxy_base_url

def get_web_services_server_base_path():
    """
    :return: the web services server base path
    """
    ws_base_url = os.environ.get('WS_BASE_URL')

    if ws_base_url is None:
        raise ContextGeneratorError(f'You need to set the WS_BASE_URL env variable!')

    return ws_base_url


def wait_util_job_finishes(delayed_jobs_base_url, job_id):
    """
    waits until the job finishes before continuing
    :param job_id: id of the jobs to check
    """
    print('Waiting until job finishes...')

    status_url = get_url_for_job_status(delayed_jobs_base_url, job_id)
    print('status_url: ', status_url)

    job_status = None

    while job_status != 'FINISHED':
        status_request = requests.get(status_url)

        print(datetime.datetime.now().isoformat())
        print('Job ID: ', job_id)
        print('Status request response code: ', status_request.status_code)

        status_response = status_request.json()
        job_status = status_response.get('status')
        job_progress = status_response.get('progress')

        print('job_status: ', job_status)
        print('job_progress: ', job_progress)
        if job_status == 'ERROR':
            raise ContextGeneratorError(f'The Job {job_id} failed!!!')
        print('---')
        time.sleep(1)


# ----------------------------------------------------------------------------------------------------------------------
# Generation of job params files
# ----------------------------------------------------------------------------------------------------------------------
def generate_job_params_file(job_id, context_type, template_filename, output_filename, additional_ctx_params=None):
    """
    Generates a job params file with the results of the search. In the params dir
    :param job_id: id of the job ran
    :param context_type: tipe of context (search) performed
    :param template_filename: filename of the template file to use
    :param output_filename: filename desired for the output
    :param additional_ctx_params: additional params for the context
    """
    print('GENERATING JOB PARAMS FILE')

    context_obj = {
        'delayed_jobs_base_url': get_delayed_jobs_server_base_path(),
        'context_type': context_type,
        'context_id': job_id
    }

    if context_type == 'BLAST':

        results_context = load_context(job_id)
        chembl_ids = [result['target_chembl_id'] for result in results_context['search_results']]
        query = {
            'bool': {
                'must': [],
                'filter': [
                    {
                        'terms': {
                            'target_chembl_id': chembl_ids
                        }
                    }
                ]
            }
        }

    elif context_type == 'TO_COMPOUNDS':
        query = {"bool": {"must": [{"query_string": {"analyze_wildcard": True, "query": "*"}}], "filter": []}}
    elif context_type == 'DIRECT_SIMILARITY':
        query = {"bool": {"must": [{"query_string": {"analyze_wildcard": True, "query": "*"}}], "filter": []}}
        context_obj = {
            'web_services_base_url': get_web_services_server_base_path(),
            'context_type': context_type,
            'context_id': job_id,
            'similarity_threshold': additional_ctx_params
        }
    else:

        results_context = load_context(job_id)
        chembl_ids = [result['molecule_chembl_id'] for result in results_context['search_results']]
        query = {
            'bool': {
                'must': [],
                'filter': [
                    {
                        'terms': {
                            'molecule_chembl_id': chembl_ids
                        }
                    }
                ]
            }
        }

    test_params_dir = 'functional_tests/params'
    template_file_path = f'{test_params_dir}/{template_filename}'
    generated_file_path = f'{test_params_dir}/{output_filename}'

    if os.path.exists(generated_file_path):
        os.remove(generated_file_path)

    with open(template_file_path, 'r') as template_file:
        template_dict = yaml.load(template_file, Loader=yaml.FullLoader)
        template_dict['job_params']['query'] = json.dumps(query)
        template_dict['job_params']['context_obj'] = json.dumps(context_obj)

        with open(generated_file_path, 'w') as generated_file:
            yaml.dump(template_dict, generated_file)

    print(f'Generated file {generated_file_path} from template {template_file_path}')


def submit_delayed_job(submission_url, payload):
    """
    Submits the delayed job to the url specified with the payload specified
    :param submission_url: url where to submit the job
    :param payload: payload for the job submission
    :return: job_id of the job submitted
    """
    submit_request = requests.post(submission_url, data=payload)
    submission_status_code = submit_request.status_code
    print(f'submission_status_code: {submission_status_code}')
    assert submission_status_code == 200, 'Job could not be submitted!'

    submission_response = submit_request.json()
    job_id = submission_response.get('job_id')
    print('job_id: ', job_id)

    return job_id


def load_context(job_id):
    """
    :param job_id: id of the job ran
    :return: The context given by the job id and type
    """
    delayed_jobs_base_url = get_delayed_jobs_server_base_path()
    job_status_url = get_url_for_job_status(delayed_jobs_base_url, job_id)
    context_url = f'http://{requests.get(job_status_url).json()["output_files_urls"]["results.json"]}'

    context = requests.get(context_url).json()
    return context


if __name__ == "__main__":
    run()
