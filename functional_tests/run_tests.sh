#!/usr/bin/env bash

set -ex

# ----------------------------------------------------------------------------------------------------------------------
# Simulate simple CSV Download
# ----------------------------------------------------------------------------------------------------------------------

rm -rf $PWD/functional_tests/output_1 || true
mkdir -p $PWD/functional_tests/output_1
PYTHONPATH=$PWD:$PYTHONPATH $PWD/download_job/run_job.py $PWD/functional_tests/params/run_params_example2.yml -n
EXIT_CODE_1=$?

if [[ $EXIT_CODE_1 -eq 0 ]]; then
    echo "The job ran successfully, as expected"
else
    echo "The job failed! Please check"
    exit 1
fi

ls -lah $PWD/functional_tests/output_1/someID.zip

# ----------------------------------------------------------------------------------------------------------------------
# Simulate simple TSV Download
# ----------------------------------------------------------------------------------------------------------------------

rm -rf $PWD/functional_tests/output_2 || true
mkdir -p $PWD/functional_tests/output_2
PYTHONPATH=$PWD:$PYTHONPATH $PWD/download_job/run_job.py $PWD/functional_tests/params/run_params_example2.yml -n
EXIT_CODE_1=$?

if [[ $EXIT_CODE_1 -eq 0 ]]; then
    echo "The job ran successfully, as expected"
else
    echo "The job failed! Please check"
    exit 1
fi

ls -lah $PWD/functional_tests/output_2/someID.zip

# ----------------------------------------------------------------------------------------------------------------------
# Simulate simple CSV Download with a different column group than the default one
# ----------------------------------------------------------------------------------------------------------------------

rm -rf $PWD/functional_tests/output_14 || true
mkdir -p $PWD/functional_tests/output_14
PYTHONPATH=$PWD:$PYTHONPATH $PWD/download_job/run_job.py $PWD/functional_tests/params/run_params_example14.yml -n
EXIT_CODE_1=$?

if [[ $EXIT_CODE_1 -eq 0 ]]; then
    echo "The job ran successfully, as expected"
else
    echo "The job failed! Please check"
    exit 1
fi

ls -lah $PWD/functional_tests/output_14/someID.zip

# ----------------------------------------------------------------------------------------------------------------------
# Simulate SDF Download with no structures
# ----------------------------------------------------------------------------------------------------------------------

rm -rf $PWD/functional_tests/output_3 || true
mkdir -p $PWD/functional_tests/output_3
PYTHONPATH=$PWD:$PYTHONPATH $PWD/download_job/run_job.py $PWD/functional_tests/params/run_params_example3.yml -n
EXIT_CODE_1=$?

if [[ $EXIT_CODE_1 -eq 0 ]]; then
    echo "The job ran successfully, as expected"
else
    echo "The job failed! Please check"
    exit 1
fi

ls -lah $PWD/functional_tests/output_3/someID.zip

# ----------------------------------------------------------------------------------------------------------------------
# Simulate SDF Download with structures
# ----------------------------------------------------------------------------------------------------------------------

rm -rf $PWD/functional_tests/output_4 || true
mkdir -p $PWD/functional_tests/output_4
PYTHONPATH=$PWD:$PYTHONPATH $PWD/download_job/run_job.py $PWD/functional_tests/params/run_params_example4.yml -n
EXIT_CODE_1=$?

if [[ $EXIT_CODE_1 -eq 0 ]]; then
    echo "The job ran successfully, as expected"
else
    echo "The job failed! Please check"
    exit 1
fi

ls -lah $PWD/functional_tests/output_4/someID.zip


# ----------------------------------------------------------------------------------------------------------------------
# Simulate CSV Download with similarity results context
# ----------------------------------------------------------------------------------------------------------------------

functional_tests/generate_context.py SIMILARITY

rm -rf $PWD/functional_tests/output_5 || true
mkdir -p $PWD/functional_tests/output_5
PYTHONPATH=$PWD:$PYTHONPATH $PWD/download_job/run_job.py $PWD/functional_tests/params/run_params_example5.yml -n
EXIT_CODE_1=$?

if [[ $EXIT_CODE_1 -eq 0 ]]; then
    echo "The job ran successfully, as expected"
else
    echo "The job failed! Please check"
    exit 1
fi

ls -lah $PWD/functional_tests/output_5/someID.zip

# ----------------------------------------------------------------------------------------------------------------------
# Simulate CSV Download with substructure results context
# ----------------------------------------------------------------------------------------------------------------------

functional_tests/generate_context.py SUBSTRUCTURE

rm -rf $PWD/functional_tests/output_6 || true
mkdir -p $PWD/functional_tests/output_6
PYTHONPATH=$PWD:$PYTHONPATH $PWD/download_job/run_job.py $PWD/functional_tests/params/run_params_example6.yml -n
EXIT_CODE_1=$?

if [[ $EXIT_CODE_1 -eq 0 ]]; then
    echo "The job ran successfully, as expected"
else
    echo "The job failed! Please check"
    exit 1
fi

ls -lah $PWD/functional_tests/output_6/someID.zip

# ----------------------------------------------------------------------------------------------------------------------
# Simulate CSV Download with connectivity results context
# ----------------------------------------------------------------------------------------------------------------------

functional_tests/generate_context.py CONNECTIVITY

rm -rf $PWD/functional_tests/output_7 || true
mkdir -p $PWD/functional_tests/output_7
PYTHONPATH=$PWD:$PYTHONPATH $PWD/download_job/run_job.py $PWD/functional_tests/params/run_params_example7.yml -n
EXIT_CODE_1=$?

if [[ $EXIT_CODE_1 -eq 0 ]]; then
    echo "The job ran successfully, as expected"
else
    echo "The job failed! Please check"
    exit 1
fi

ls -lah $PWD/functional_tests/output_7/someID.zip

# ----------------------------------------------------------------------------------------------------------------------
# Simulate CSV Download with BLAST results context
# ----------------------------------------------------------------------------------------------------------------------

functional_tests/generate_context.py BLAST

rm -rf $PWD/functional_tests/output_8 || true
mkdir -p $PWD/functional_tests/output_8
PYTHONPATH=$PWD:$PYTHONPATH $PWD/download_job/run_job.py $PWD/functional_tests/params/run_params_example8.yml -n
EXIT_CODE_1=$?

if [[ $EXIT_CODE_1 -eq 0 ]]; then
    echo "The job ran successfully, as expected"
else
    echo "The job failed! Please check"
    exit 1
fi

ls -lah $PWD/functional_tests/output_8/someID.zip

# ----------------------------------------------------------------------------------------------------------------------
# Simulate SDF Download with a big file
# ----------------------------------------------------------------------------------------------------------------------

rm -rf $PWD/functional_tests/output_9 || true
mkdir -p $PWD/functional_tests/output_9
PYTHONPATH=$PWD:$PYTHONPATH $PWD/download_job/run_job.py $PWD/functional_tests/params/run_params_example9.yml -n
EXIT_CODE_1=$?

if [[ $EXIT_CODE_1 -eq 0 ]]; then
    echo "The job ran successfully, as expected"
else
    echo "The job failed! Please check"
    exit 1
fi

ls -lah $PWD/functional_tests/output_9/someID.zip

# ----------------------------------------------------------------------------------------------------------------------
# Simulate CSV Download with search by IDs results context
# ----------------------------------------------------------------------------------------------------------------------

functional_tests/generate_context.py SEARCH_BY_IDS

rm -rf $PWD/functional_tests/output_10 || true
mkdir -p $PWD/functional_tests/output_10
PYTHONPATH=$PWD:$PYTHONPATH $PWD/download_job/run_job.py $PWD/functional_tests/params/run_params_example10.yml -n
EXIT_CODE_1=$?

if [[ $EXIT_CODE_1 -eq 0 ]]; then
    echo "The job ran successfully, as expected"
else
    echo "The job failed! Please check"
    exit 1
fi

ls -lah $PWD/functional_tests/output_10/someID.zip

# ----------------------------------------------------------------------------------------------------------------------
# Simulate SDF Download with search by IDs results context
# ----------------------------------------------------------------------------------------------------------------------
functional_tests/generate_context.py SEARCH_BY_IDS_SDF

rm -rf $PWD/functional_tests/output_11 || true
mkdir -p $PWD/functional_tests/output_11
PYTHONPATH=$PWD:$PYTHONPATH $PWD/download_job/run_job.py $PWD/functional_tests/params/run_params_example11.yml -n
EXIT_CODE_1=$?

if [[ $EXIT_CODE_1 -eq 0 ]]; then
    echo "The job ran successfully, as expected"
else
    echo "The job failed! Please check"
    exit 1
fi

ls -lah $PWD/functional_tests/output_11/someID.zip

# ----------------------------------------------------------------------------------------------------------------------
# Simulate simple CSV Download with direct similarity search context.
# ----------------------------------------------------------------------------------------------------------------------
functional_tests/generate_context.py DIRECT_SIMILARITY

rm -rf $PWD/functional_tests/output_12 || true
mkdir -p $PWD/functional_tests/output_12
PYTHONPATH=$PWD:$PYTHONPATH $PWD/download_job/run_job.py $PWD/functional_tests/params/run_params_example12.yml -n
EXIT_CODE_1=$?

if [[ $EXIT_CODE_1 -eq 0 ]]; then
    echo "The job ran successfully, as expected"
else
    echo "The job failed! Please check"
    exit 1
fi

ls -lah $PWD/functional_tests/output_12/someID.zip

# ----------------------------------------------------------------------------------------------------------------------
# Simulate SDF Download with direct similarity search context.
# ----------------------------------------------------------------------------------------------------------------------
functional_tests/generate_context.py DIRECT_SIMILARITY_SDF

rm -rf $PWD/functional_tests/output_13 || true
mkdir -p $PWD/functional_tests/output_13
PYTHONPATH=$PWD:$PYTHONPATH $PWD/download_job/run_job.py $PWD/functional_tests/params/run_params_example13.yml -n
EXIT_CODE_1=$?

if [[ $EXIT_CODE_1 -eq 0 ]]; then
    echo "The job ran successfully, as expected"
else
    echo "The job failed! Please check"
    exit 1
fi

ls -lah $PWD/functional_tests/output_13/someID.zip