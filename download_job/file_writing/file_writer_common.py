"""
Common functions shared between the file writers
"""


def get_file_object_for_part(zip_archive, base_filename, part_number, extension):
    """
    :param zip_archive: zip archive where the files will be saved
    :param base_filename: base filename to use
    :param part_number: number of the part
    :param extension: extension of the file to generate
    :return: file object to use for the part entered as a parameter
    """
    if part_number == 1:
        zip_internal_file_name = f'{base_filename}.{extension}'
    else:
        zip_internal_file_name = f'{base_filename}_part{part_number}.{extension}'

    out_file = zip_archive.open(zip_internal_file_name, 'w')
    return out_file
