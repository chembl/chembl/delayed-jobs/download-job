"""
Module with parsing functions for the different properties if needed
"""


def parse_synonyms(raw_synonyms):
    """
    :param raw_synonyms:
    :return:
    """
    if raw_synonyms is None:
        return ''
    true_synonyms = set()
    for raw_syn in raw_synonyms:
        true_synonyms.add(raw_syn['synonyms'])
    sorted_synonyms = list(true_synonyms)
    sorted_synonyms.sort()
    return '|'.join(sorted_synonyms)


def parse_research_codes(raw_synonyms):
    """
    :param raw_synonyms:
    :return:
    """
    if raw_synonyms is None:
        return ''
    true_synonyms = set()
    for raw_syn in raw_synonyms:
        if raw_syn['syn_type'] == 'RESEARCH_CODE':
            true_synonyms.add(raw_syn['molecule_synonym'])
    sorted_synonyms = list(true_synonyms)
    sorted_synonyms.sort()
    return '|'.join(sorted_synonyms)


def list_to_pipe_separated_string(raw_applicants):
    """
    :param raw_applicants:
    :return:
    """
    if raw_applicants is None:
        return ''
    return '|'.join(raw_applicants)


def escape_text_with_simple_colon(raw_value):
    """
    :param raw_value:
    :return:
    """
    if raw_value is None:
        return ''
    if raw_value == '':
        return ''
    return "'{}'".format(raw_value)


def parse_atc_codes(raw_classifications):
    """
    :param raw_classifications:
    :return:
    """
    if raw_classifications is None:
        return ''
    return ' | '.join([class_data['level5'] for class_data in raw_classifications])


def parse_atc_codes_descriptions(raw_classifications, level):
    """
    :param raw_classifications:
    :param level:
    :return:
    """
    if raw_classifications is None:
        return ''
    return ' | '.join([class_data['level' + str(level) + '_description'] for class_data in raw_classifications])


def parse_assay_classifications(raw_classifications, level):
    """
    :param raw_classifications:
    :param level:
    :return:
    """
    if raw_classifications is None:
        return ''
    return ' | '.join([class_data['l' + str(level)] for class_data in raw_classifications])


def parse_drug_atc_class_descriptions(raw_classifications):
    """
    :param raw_classifications:
    :return:
    """
    if raw_classifications is None:
        return ''
    return '|'.join([class_data['description'] for class_data in raw_classifications])


def parse_drug_type(raw_type):
    """
    :param raw_type:
    :return:
    """
    parsed_type = int(raw_type)

    parsed_type_to_text = {
        1: '1:Synthetic Small Molecule',
        2: '2:Enzyme',
        3: '3:Oligosaccharide',
        4: '4:Oligonucleotide',
        5: '5:Oligopeptide',
        6: '6:Antibody',
        7: '7:Natural Product-derived',
        8: '8:Cell-based',
        9: '9:Inorganic',
        10: '10:Polymer',
        -1: '-1:Unknown'
    }
    text = parsed_type_to_text.get(parsed_type)
    if text is not None:
        return text
    return ''


def parse_target_uniprot_accession(raw_components):
    """
    :param raw_components:
    :return:
    """
    if raw_components is None:
        return ''
    accessions = []
    for comp in raw_components:
        accession = comp.get('accession')
        if accession is not None:
            accessions.append(accession)

    return '|'.join(accessions)


def parse_mech_of_act_synonyms(raw_synonyms):
    """
    :param raw_synonyms:
    :return:
    """
    if raw_synonyms is None:
        return ''
    return '|'.join(raw_synonyms)


def parse_document_source(raw_source):
    """
    :param raw_source:
    :return:
    """
    if raw_source is None:
        return ''
    return '|'.join([v['src_description'] for v in raw_source])


def parse_efo_ids(raw_efo_data):
    """
    :param raw_efo_data:
    :return:
    """
    if raw_efo_data is None:
        return ''
    return '|'.join([v['id'] for v in raw_efo_data])


def parse_efo_efo_terms(raw_efo_data):
    """
    :param raw_efo_data:
    :return:
    """
    if raw_efo_data is None:
        return ''
    return '|'.join([v['term'] for v in raw_efo_data])


def parse_chembl_refs(raw_refs):
    """
    :param raw_refs:
    :return:
    """
    if raw_refs is None:
        return ''
    return '|'.join([f'Type: {r["ref_type"]} RefID: {r["ref_id"]} URL: {r["ref_url"]}' for r in raw_refs])


def parse_mech_or_indication_syns(raw_synonyms):
    """
    :param raw_synonyms:
    :return:
    """
    if raw_synonyms is None:
        return ''
    return '|'.join(raw_synonyms)


def parse_assay_parameters(raw_parameters):
    """
    :param raw_parameters:
    :return:
    """
    if raw_parameters is None:
        return ''
    params_groups = []

    for param in raw_parameters:

        useful_params = []

        for key in ['standard_type', 'standard_relation', 'standard_value', 'standard_units', 'comments']:
            useful_params.append('{param_name}:{value}'.format(param_name=key, value=param[key]))

        params_groups.append(' '.join(useful_params))

    return ' | '.join(params_groups)


def parse_assay_and_activity_properties_in_activity(raw_values):
    """
    :param raw_values: the raw values from elasticsearch
    :return: the text value of the object
    """

    if raw_values is None:
        return ''

    if raw_values == '':
        return ''

    parameters_texts = []
    for current_param in raw_values:

        standard_value = current_param.get('standard_value')
        standard_type = current_param.get('standard_type', '')

        if standard_value is None or standard_value == '':
            standard_text_value = current_param.get('standard_text_value', '')
            param_text = f'{standard_type}: {standard_text_value}'
        else:
            standard_relation = current_param.get('standard_relation', '')
            standard_value = current_param.get('standard_value', '')
            standard_units = current_param.get('standard_units', '')
            param_text = f'{standard_type} {standard_relation} {standard_value} {standard_units}'

        parameters_texts.append(param_text)

    return ' | '.join(parameters_texts)


def parse_drug_warning_wheres(wheres):
    """
    Parses the drug warning wheres
    :param wheres: list of wheres
    :return: the text to include in the value
    """
    if wheres is None:
        return ''
    if len(wheres) == 0:
        return ''

    where_texts = []
    for where in wheres:
        where_text = f'{where.get("country", "No country")}: {where.get("year", "No year")}'
        where_texts.append(where_text)

    return ','.join(where_texts)


PARSING_FUNCTIONS = {
    'chembl_molecule': {
        'molecule_synonyms': parse_synonyms,
        'research_codes': parse_research_codes,
        '_metadata.drug.drug_data.applicants': list_to_pipe_separated_string,
        'usan_stem': escape_text_with_simple_colon,
        '_metadata.drug.drug_data.usan_stem_substem': escape_text_with_simple_colon,
        'drug_atc_descriptions': parse_drug_atc_class_descriptions,
        '_metadata.drug.drug_data.drug_type': parse_drug_type,
        'drug_atc_codes': parse_atc_codes,
        'drug_atc_codes_level_4': lambda original_value: parse_atc_codes_descriptions(original_value, 4),
        'drug_atc_codes_level_3': lambda original_value: parse_atc_codes_descriptions(original_value, 3),
        'drug_atc_codes_level_2': lambda original_value: parse_atc_codes_descriptions(original_value, 2),
        'drug_atc_codes_level_1': lambda original_value: parse_atc_codes_descriptions(original_value, 1),
    },
    'chembl_target': {
        'uniprot_accessions': parse_target_uniprot_accession
    },
    'chembl_assay': {
        'assay_parameters': parse_assay_parameters,
        'assay_classifications_level1': lambda original_value: parse_assay_classifications(original_value, 1),
        'assay_classifications_level2': lambda original_value: parse_assay_classifications(original_value, 2),
        'assay_classifications_level3': lambda original_value: parse_assay_classifications(original_value, 3),
    },
    'chembl_document': {
        '_metadata.source': parse_document_source,
    },
    'chembl_activity': {
        'standard_relation': escape_text_with_simple_colon,
        '_metadata.assay_data.assay_parameters': parse_assay_and_activity_properties_in_activity,
        'activity_properties': parse_assay_and_activity_properties_in_activity
    },
    'chembl_drug_indication_by_parent': {
        'parent_molecule._metadata.drug.drug_data.synonyms': parse_mech_of_act_synonyms,
        'efo_ids': parse_efo_ids,
        'efo_terms': parse_efo_efo_terms,
        'drug_indication.indication_refs': parse_chembl_refs,
        'parent_molecule._metadata.drug.drug_data.usan_stem': escape_text_with_simple_colon
    },
    'chembl_mechanism_by_parent_target': {
        'parent_molecule.usan_stem': escape_text_with_simple_colon,
        'mechanism_of_action.mechanism_comment': list_to_pipe_separated_string,
        'mechanism_of_action.selectivity_comment': list_to_pipe_separated_string,
        'mechanism_of_action.mechanism_refs': parse_chembl_refs,
        'parent_molecule._metadata.drug.drug_data.synonyms': parse_mech_or_indication_syns,
        'mechanism_of_action.binding_site_comment': list_to_pipe_separated_string,
        'drug_atc_codes': parse_atc_codes,
        'drug_atc_codes_level_4': lambda original_value: parse_atc_codes_descriptions(original_value, 4),
        'drug_atc_codes_level_3': lambda original_value: parse_atc_codes_descriptions(original_value, 3),
        'drug_atc_codes_level_2': lambda original_value: parse_atc_codes_descriptions(original_value, 2),
        'drug_atc_codes_level_1': lambda original_value: parse_atc_codes_descriptions(original_value, 1),
    },
    'chembl_drug_warning_by_parent': {
        'drug_warning.where': parse_drug_warning_wheres,
        'drug_warning.warning_refs': parse_chembl_refs,
    },
    'chembl_eubopen_activity': {
        '_metadata.eubopen.assay_type.type.description': list_to_pipe_separated_string,
    },
    'chembl_eubopen_assay': {
        '_metadata.eubopen.assay_type.type.description': list_to_pipe_separated_string,
    },
    'chembl_eubopen_molecule': {
        'molecule_synonyms': parse_synonyms,
    }

}


def parse(original_value, index_name, property_name):
    """
    :param original_value: the original value found
    :param index_name: the index name to which the property belongs
    :param property_name: id of the property to which the value corresponds
    :return: the parsed value
    """
    functions_for_index = PARSING_FUNCTIONS.get(index_name)

    if functions_for_index is None:
        return original_value

    function_for_property = functions_for_index.get(property_name)
    if function_for_property is None:
        return original_value

    return function_for_property(original_value)
