"""
Module that writes SDF files after collecting data from ES
"""
import os
import zipfile
from pathlib import Path
import sys

from elasticsearch.helpers import scan

from download_job.file_writing import es_connection
from download_job.file_writing.utils import dict_property_access
from download_job.file_writing import file_writer_common


class SDFFileWriter:
    """
    Class that writes files on the initialisation parameters
    """

    class SDFFileWriterError(Exception):
        """Base class for exceptions in the file writer."""

    def __init__(self, query, data_index=None):
        """
        Writes a file in the SDF formant from the chembl_molecule index
        :param query: query to send to ES
        :param data_index: index where to take the data from, in case it is different from chembl_molecule. This would
        be the case of search by IDs
        """
        self.query = query
        self.index_name = 'chembl_molecule'
        self.data_index = data_index
        if self.data_index is None:
            self.data_index = self.index_name

        self.es_conn = es_connection.get_es_connection_from_env_config()

    def get_total_items(self):
        """
        :return: the total items to download
        """
        return self.es_conn.search(
            index=self.data_index,
            body={'query': self.query, 'track_total_hits': True}
        )['hits']['total']['value']

    def get_scanner(self):
        """
        :return: the scanner to get the data
        """
        return scan(
            self.es_conn,
            index=self.data_index,
            scroll=u'1m',
            size=1000,
            request_timeout=60,
            query={
                "_source": ['molecule_structures.molfile'],
                "query": self.query
            }
        )

    def write_sdf_file(self, output_dir_path, base_filename, progress_function=(lambda progress: progress)):
        """
        Writes the file in the path set in the parameter with a filename based on the parameter
        :param output_dir_path: directory where to write the file
        :param base_filename: base name of the file
        :param progress_function: function to be called when as the progress changes
        :return: the path, and the total items included in the path
        """

        Path(output_dir_path).mkdir(parents=True, exist_ok=True)
        output_file_path = os.path.join(output_dir_path, base_filename + '.zip')

        total_items = self.get_total_items()
        scanner = self.get_scanner()

        with zipfile.ZipFile(output_file_path, 'w', zipfile.ZIP_DEFLATED, allowZip64=True) as zip_archive:

            # Make a new part every 1 GB inside the zip archive.
            partition_size = int(os.environ.get('MAX_FILE_BYTES', 1000000000))
            part_number = 1

            out_file = file_writer_common.get_file_object_for_part(zip_archive, base_filename, part_number, 'sdf')

            i = 0
            previous_percentage = 0
            progress_function(previous_percentage)
            num_items_with_structure = 0
            total_estimated_bytes_written = 0
            estimated_bytes_written = 0
            for doc_i in scanner:
                i += 1

                doc_source = doc_i['_source']
                sdf_value = dict_property_access.get_property_value(doc_source, 'molecule_structures.molfile', '')

                if sdf_value is None or sdf_value == '':
                    continue

                encoded_sdf_value = sdf_value.encode()
                encoded_value_size = sys.getsizeof(encoded_sdf_value)
                total_estimated_bytes_written += encoded_value_size
                estimated_bytes_written += encoded_value_size

                if estimated_bytes_written > partition_size:
                    # Switch to a new part
                    part_number += 1
                    estimated_bytes_written = 0
                    out_file.close()
                    out_file = file_writer_common.get_file_object_for_part(zip_archive, base_filename, part_number,
                                                                           'sdf')

                out_file.write(encoded_sdf_value)
                out_file.write('\n\n$$$$\n'.encode())
                num_items_with_structure += 1

                percentage = int((i / total_items) * 100)
                if percentage != previous_percentage:
                    previous_percentage = percentage
                    progress_function(percentage)
                    print(f'part_number: {part_number}')
                    print(f'total_estimated_bytes_written: {total_estimated_bytes_written}')
                    print(f'estimated_bytes_written: {estimated_bytes_written}')

            print(f'num_items_with_structure: {num_items_with_structure}')
            if num_items_with_structure == 0:
                print('There are no items with structures for the SDF files')
                out_file.write('None of the downloaded items have a chemical structure,'
                               ' please try other download formats.\n'.encode())

            out_file.close()

        return output_file_path, num_items_with_structure
