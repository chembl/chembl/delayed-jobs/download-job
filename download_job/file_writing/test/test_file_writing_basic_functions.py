"""
Module to test the file writer basic functions
"""
import unittest
import os
import json
import time
import zipfile

from download_job.file_writing.file_writer import FileWriter


def get_basic_csv_file_writer():
    """
    :return: a basic file writer for testing
    """

    columns_to_download = [{'label': 'ChEMBL ID', 'prop_id': 'molecule_chembl_id'},
                           {'label': 'Name', 'prop_id': 'pref_name'}]
    index_name = 'chembl_molecule'
    desired_format = FileWriter.OutputFormats.CSV

    query_file_path = os.path.join('download_job/file_writing/test/data/test_query0.json')
    with open(query_file_path, 'rt') as query_file:
        test_query = json.loads(query_file.read())

        file_writer = FileWriter(
            desired_format=desired_format,
            index_name=index_name,
            query=test_query,
            columns_to_download=columns_to_download,
        )

        return file_writer


def get_basic_csv_file_writer_2():
    """
    :return: a basic file writer for testing in which parsing is required
    """

    columns_to_download = [{'label': 'ChEMBL ID', 'prop_id': 'molecule_chembl_id'},
                           {'label': 'Synonyms', 'prop_id': 'molecule_synonyms'}]
    index_name = 'chembl_molecule'
    desired_format = FileWriter.OutputFormats.CSV

    query_file_path = os.path.join('download_job/file_writing/test/data/test_query0.json')
    with open(query_file_path, 'rt') as query_file:
        test_query = json.loads(query_file.read())

        file_writer = FileWriter(
            desired_format=desired_format,
            index_name=index_name,
            query=test_query,
            columns_to_download=columns_to_download,
        )

        return file_writer


def get_basic_csv_file_writer_with_wirtual_properties():
    """
    :return: a basic file writer for testing
    """

    columns_to_download = [{'label': 'ChEMBL ID', 'prop_id': 'molecule_chembl_id'},
                           {'label': 'Research Codes', 'prop_id': 'research_codes', 'is_virtual': True,
                            'is_contextual': True, 'based_on': 'molecule_synonyms'}]

    index_name = 'chembl_molecule'
    desired_format = FileWriter.OutputFormats.CSV

    query_file_path = os.path.join('download_job/file_writing/test/data/test_query0.json')
    with open(query_file_path, 'rt') as query_file:
        test_query = json.loads(query_file.read())

        file_writer = FileWriter(
            desired_format=desired_format,
            index_name=index_name,
            query=test_query,
            columns_to_download=columns_to_download,
        )

        return file_writer


def get_test_output_dir_path():
    """
    :return: the path of the directoy to use for the tests
    """
    return 'download_job/file_writing/test/test_output'


class FileWriterTesterBasicFunctions(unittest.TestCase):
    """
    Class to test the file writer basic functions
    """

    def test_generates_source(self):
        """
        Tests that generates the correct source configuration
        """
        file_writer = get_basic_csv_file_writer()

        source_must_be = ['molecule_chembl_id', 'pref_name']
        source_got = file_writer.get_search_source()

        self.assertEqual(source_must_be, source_got, 'The search source is not generated correctly')

    def test_generates_source_for_virtual_properties(self):
        """
        Tests that generates the correct source configuration from virtual properties
        """

        file_writer = get_basic_csv_file_writer_with_wirtual_properties()
        source_must_be = ['molecule_chembl_id', 'molecule_synonyms']
        source_got = file_writer.get_search_source()
        self.assertEqual(source_must_be, source_got, 'The search source is not generated correctly')

    def test_fails_when_output_format_is_not_available(self):
        """
        Tests that fails when output format is not available
        """

        columns_to_download = [{'label': 'ChEMBL ID', 'prop_id': 'molecule_chembl_id'},
                               {'label': 'Name', 'prop_id': 'pref_name'}]
        index_name = 'chembl_molecule'
        desired_format = 'NOT_SUPPORTED'

        query_file_path = os.path.join('download_job/file_writing/test/data/test_query0.json')
        with open(query_file_path, 'rt') as query_file:
            test_query = json.loads(query_file.read())

            with self.assertRaises(FileWriter.FileWriterError,
                                   msg='It should raise an error when the given format is not supported'):
                FileWriter(
                    desired_format=desired_format,
                    index_name=index_name,
                    query=test_query,
                    columns_to_download=columns_to_download,
                )

    def test_fails_when_index_name_is_not_provided(self):
        """
        Tests it fails when the index name is not provided
        """

        columns_to_download = [{'label': 'ChEMBL ID', 'prop_id': 'molecule_chembl_id'},
                               {'label': 'Name', 'prop_id': 'pref_name'}]
        index_name = None
        desired_format = FileWriter.OutputFormats.CSV

        query_file_path = os.path.join('download_job/file_writing/test/data/test_query0.json')
        with open(query_file_path, 'rt') as query_file:
            test_query = json.loads(query_file.read())

            with self.assertRaises(FileWriter.FileWriterError,
                                   msg='It should raise an error when the index name is not given'):
                FileWriter(
                    desired_format=desired_format,
                    index_name=index_name,
                    query=test_query,
                    columns_to_download=columns_to_download,
                )

    def test_downloads_and_writes_csv_file_no_parsing_required(self):
        """
        Test that downloads and writes a csv file when no parsing is required
        """
        file_writer = get_basic_csv_file_writer()
        output_dir_path = get_test_output_dir_path()
        base_filename = 'test' + str(int(round(time.time() * 1000)))

        out_file_path = file_writer.write_separated_values_file(output_dir_path, base_filename)[0]

        with zipfile.ZipFile(out_file_path) as zip_got:
            filename_got = [filename for filename in zip_got.namelist() if not os.path.isdir(filename)][0]

            with zip_got.open(filename_got, 'r') as file_got:
                lines_got = file_got.readlines()
                line_0 = lines_got[0].decode()
                self.assertEqual(line_0, '"ChEMBL ID";"Name"\n', 'Header line is malformed!')
                line_1 = lines_got[1].decode()
                self.assertEqual(line_1, '"CHEMBL59";"DOPAMINE"\n', 'Line is malformed!')

    def test_downloads_and_writes_csv_file_parsing_required(self):
        """
        Test that downloads and writes a csv file when parsing is required
        """
        file_writer = get_basic_csv_file_writer_2()
        output_dir_path = get_test_output_dir_path()
        base_filename = 'test' + str(int(round(time.time() * 1000)))

        out_file_path = file_writer.write_separated_values_file(output_dir_path, base_filename)[0]

        with zipfile.ZipFile(out_file_path) as zip_got:
            filename_got = [filename for filename in zip_got.namelist() if not os.path.isdir(filename)][0]

            with zip_got.open(filename_got, 'r') as file_got:
                lines_got = file_got.readlines()
                line_0 = lines_got[0].decode()
                self.assertEqual(line_0, '"ChEMBL ID";"Synonyms"\n', 'Header line is malformed!')
                line_1 = lines_got[1].decode()
                self.assertEqual(line_1,
                                 '"CHEMBL59";"A-DOPAMINE|Carbilev|DOBUTAMINE A|DOPAMINE|Dopamine|Intropin|MEDOPA|'
                                 'NSC-173182|OXYTYRAMINE|Parcopa|Sinemet"\n',
                                 'Line is malformed!')
