"""
Module to test the file writer
"""
import unittest
import os
import json
import time
import zipfile

from download_job.file_writing.file_writer import FileWriter


def get_basic_csv_file_writer_with_context():
    """
    :return: a basic file writer for testing in which parsing is required
    """

    columns_to_download = [{'label': 'ChEMBL ID', 'prop_id': 'molecule_chembl_id'},
                           {'label': 'Synonyms', 'prop_id': 'molecule_synonyms'}]
    index_name = 'chembl_molecule'
    desired_format = FileWriter.OutputFormats.CSV

    query_file_path = os.path.join('download_job/file_writing/test/data/test_query0.json')
    with open(query_file_path, 'rt') as query_file:
        test_query = json.loads(query_file.read())

        contextual_columns = [{'label': 'Similarity', 'prop_id': 'similarity'}]
        id_property = 'molecule_chembl_id'
        context = {
            'CHEMBL59': {
                'molecule_chembl_id': 'ChEMBL59',
                'similarity': 100.0
            }
        }

        file_writer = FileWriter(
            desired_format=desired_format,
            index_name=index_name,
            query=test_query,
            columns_to_download=columns_to_download,
            contextual_columns=contextual_columns,
            id_property=id_property,
            context=context
        )

        return file_writer


def get_basic_csv_file_writer_with_context_and_virtual_properties():
    """
    :return: a basic file writer for testing in which parsing is required
    """
    columns_to_download = [{'label': 'ChEMBL ID', 'prop_id': 'molecule_chembl_id'},
                           {'label': 'Research Codes', 'prop_id': 'research_codes', 'is_virtual': True,
                            'is_contextual': False, 'based_on': 'molecule_synonyms'}]
    index_name = 'chembl_molecule'
    desired_format = FileWriter.OutputFormats.CSV

    query_file_path = os.path.join('download_job/file_writing/test/data/test_query0.json')
    with open(query_file_path, 'rt') as query_file:
        test_query = json.loads(query_file.read())

        contextual_columns = [{'label': 'Similarity', 'prop_id': 'similarity'}]
        id_property = 'molecule_chembl_id'
        context = {
            'CHEMBL59': {
                'molecule_chembl_id': 'ChEMBL59',
                'similarity': 100.0
            }
        }

        file_writer = FileWriter(
            desired_format=desired_format,
            index_name=index_name,
            query=test_query,
            columns_to_download=columns_to_download,
            contextual_columns=contextual_columns,
            id_property=id_property,
            context=context
        )

        return file_writer


def get_test_output_dir_path():
    """
    :return: the path of the directoy to use for the tests
    """
    return 'download_job/file_writing/test/test_output'


class FileWriterTesterBasic(unittest.TestCase):
    """
    Class to test the file writer
    """

    def test_fails_with_context_but_no_id_property(self):
        """
        Test it fails when context is provided but no id property
        """

        columns_to_download = [{'label': 'ChEMBL ID', 'prop_id': 'molecule_chembl_id'},
                               {'label': 'Synonyms', 'prop_id': 'molecule_synonyms'}]
        index_name = 'chembl_molecule'
        desired_format = FileWriter.OutputFormats.CSV

        query_file_path = os.path.join('download_job/file_writing/test/data/test_query0.json')
        with open(query_file_path, 'rt') as query_file:
            test_query = json.loads(query_file.read())

            contextual_columns = [{'label': 'Similarity', 'prop_id': 'similarity'}]
            context = {
                'ChEMBL59': {
                    'molecule_chembl_id': 'ChEMBL59',
                    'similarity': 100.0
                }
            }

            with self.assertRaises(FileWriter.FileWriterError,
                                   msg='It should raise an error when the context is given but no id property'):
                FileWriter(
                    desired_format=desired_format,
                    index_name=index_name,
                    query=test_query,
                    columns_to_download=columns_to_download,
                    contextual_columns=contextual_columns,
                    context=context
                )

    def test_fails_with_context_but_no_contextual_columns(self):
        """
        Test it fails when context is set up but no contextual columns
        """
        columns_to_download = [{'label': 'ChEMBL ID', 'prop_id': 'molecule_chembl_id'},
                               {'label': 'Synonyms', 'prop_id': 'molecule_synonyms'}]
        index_name = 'chembl_molecule'
        desired_format = FileWriter.OutputFormats.CSV

        query_file_path = os.path.join('download_job/file_writing/test/data/test_query0.json')
        with open(query_file_path, 'rt') as query_file:
            test_query = json.loads(query_file.read())

            id_property = 'molecule_chembl_id'
            context = {
                'ChEMBL59': {
                    'molecule_chembl_id': 'ChEMBL59',
                    'similarity': 100.0
                }
            }

            with self.assertRaises(FileWriter.FileWriterError,
                                   msg='It should raise an error when the context is given but no contextual columns'):
                FileWriter(
                    desired_format=desired_format,
                    index_name=index_name,
                    query=test_query,
                    columns_to_download=columns_to_download,
                    id_property=id_property,
                    context=context
                )

    def test_writes_csv_files_with_context(self):
        """
        Test it writes csv files with context
        """

        file_writer = get_basic_csv_file_writer_with_context()
        output_dir_path = get_test_output_dir_path()
        base_filename = 'test' + str(int(round(time.time() * 1000)))

        out_file_path = file_writer.write_separated_values_file(output_dir_path, base_filename)[0]

        with zipfile.ZipFile(out_file_path) as zip_got:
            filename_got = [filename for filename in zip_got.namelist() if not os.path.isdir(filename)][0]

            with zip_got.open(filename_got, 'r') as file_got:
                lines_got = file_got.readlines()
                line_0 = lines_got[0].decode()
                self.assertEqual(line_0, '"Similarity";"ChEMBL ID";"Synonyms"\n', 'Header line is malformed!')
                line_1 = lines_got[1].decode()
                self.assertEqual(
                    line_1,
                    '"100.0";"CHEMBL59";"A-DOPAMINE|Carbilev|DOBUTAMINE A|DOPAMINE|Dopamine|Intropin|MEDOPA|'
                    'NSC-173182|OXYTYRAMINE|Parcopa|Sinemet"\n',
                    'Line is malformed!')

    def test_writes_csv_files_with_virtual_properties(self):
        """
        Test writes CSV files with virtual properties
        """
        file_writer = get_basic_csv_file_writer_with_context_and_virtual_properties()
        output_dir_path = get_test_output_dir_path()
        base_filename = 'test' + str(int(round(time.time() * 1000)))

        out_file_path = file_writer.write_separated_values_file(output_dir_path, base_filename)[0]

        with zipfile.ZipFile(out_file_path) as zip_got:
            filename_got = [filename for filename in zip_got.namelist() if not os.path.isdir(filename)][0]

            with zip_got.open(filename_got, 'r') as file_got:
                lines_got = file_got.readlines()
                line_0 = lines_got[0].decode()
                self.assertEqual(line_0, '"Similarity";"ChEMBL ID";"Research Codes"\n', 'Header line is malformed!')
                line_1 = lines_got[1].decode()
                self.assertEqual(line_1, '"100.0";"CHEMBL59";"NSC-173182"\n',
                                 'Line is malformed!')

    def test_reports_file_writing_progress(self):
        """
        Test reports file writing progress
        """

        file_writer = get_basic_csv_file_writer_with_context()
        progress_got = []

        def progress_function(progress):
            progress_got.append(progress)

        output_dir_path = get_test_output_dir_path()
        base_filename = 'test' + str(int(round(time.time() * 1000)))
        file_writer.write_separated_values_file(output_dir_path, base_filename, progress_function)

        is_ascending = all(progress_got[i] <= progress_got[i + 1] for i in range(len(progress_got) - 1))
        ends_with100 = progress_got[-1] == 100

        self.assertTrue(is_ascending, msg='The progress is not reported correctly, it should be ascending. '
                                          'I got this: {}\n'.format(str(progress_got)))

        self.assertTrue(ends_with100, msg='The progress must end with 100. I got this: {}\n'.format(
            str(progress_got)))
