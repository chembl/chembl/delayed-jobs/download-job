"""
Module to test the file writer for SDF files
"""
import unittest
import os
import json
import time
import zipfile

from download_job.file_writing.sdf_file_writer import SDFFileWriter


def get_basic_sdf_file_writer():
    """
    :return: a basic file writer for testing sdf files
    """

    query_file_path = os.path.join('download_job/file_writing/test/data/test_query0.json')
    with open(query_file_path, 'rt') as query_file:
        test_query = json.loads(query_file.read())

        file_writer = SDFFileWriter(query=test_query)

        return file_writer


def get_basic_sdf_file_writer_where_some_items_have_no_structures():
    """
    :return: a basic file writer for testing sdf files
    """

    query_file_path = os.path.join('download_job/file_writing/test/data/test_query3.json')
    with open(query_file_path, 'rt') as query_file:
        test_query = json.loads(query_file.read())

        file_writer = SDFFileWriter(query=test_query)

        return file_writer


def get_basic_sdf_file_writer_where_no_items_have_structure():
    """
    :return: a basic file writer for testing sdf files
    """

    query_file_path = os.path.join('download_job/file_writing/test/data/test_query4.json')
    with open(query_file_path, 'rt') as query_file:
        test_query = json.loads(query_file.read())

        file_writer = SDFFileWriter(query=test_query)

        return file_writer


def get_test_output_dir_path():
    """
    :return: the path of the directoy to use for the tests
    """
    return 'download_job/file_writing/test/test_output'


class FileWriterTesterSDF(unittest.TestCase):
    """
    Class to test the file writer for sdf files
    """

    def test_writes_simple_sdf_file(self):
        """
        Tests it writes a simple sdf file
        """
        file_writer = get_basic_sdf_file_writer()

        output_dir_path = get_test_output_dir_path()
        base_filename = 'test' + str(int(round(time.time() * 1000)))

        out_file_path, total_items = file_writer.write_sdf_file(output_dir_path, base_filename)

        sdf_must_be_path = os.path.join('download_job/file_writing/test/data/simple_sdf_must_be.sdf')

        with open(sdf_must_be_path, 'rt') as sdf_file_must_be:
            sdf_must_be = sdf_file_must_be.read()

            with zipfile.ZipFile(out_file_path) as zip_got:
                filename_got = [filename for filename in zip_got.namelist() if not os.path.isdir(filename)][0]

                num_items_must_be = 1
                with zip_got.open(filename_got, 'r') as file_got:
                    content_got = file_got.read().decode()
                    print(
                        '---SDF_IS------------------------------------------------------------------\n' +
                        '---PATH: ' + sdf_must_be_path + '\n' +
                        content_got + '\n' +
                        '---END---------------------------------------------------------------------'
                    )
                    self.assertEqual(sdf_must_be, content_got, msg='The sdf was not generated properly!')
                    self.assertEqual(num_items_must_be, total_items,
                                     msg='The total number of items was not returned properly')

    def test_writes_simple_sdf_file_when_some_items_have_no_structure(self):
        """
        Test that it writes a simple sdf file when some items have no structure
        """

        file_writer = get_basic_sdf_file_writer_where_some_items_have_no_structures()

        output_dir_path = get_test_output_dir_path()
        base_filename = 'test' + str(int(round(time.time() * 1000)))

        out_file_path, total_items = file_writer.write_sdf_file(output_dir_path, base_filename)

        sdf_must_be_path = os.path.join('download_job/file_writing/test/data/simple_sdf_must_be.sdf')

        with open(sdf_must_be_path, 'rt') as sdf_file_must_be:
            sdf_must_be = sdf_file_must_be.read()
            num_items_must_be = 1

            with zipfile.ZipFile(out_file_path) as zip_got:
                filename_got = [filename for filename in zip_got.namelist() if not os.path.isdir(filename)][0]

                with zip_got.open(filename_got, 'r') as file_got:
                    content_got = file_got.read().decode()
                    print(
                        '---SDF_IS------------------------------------------------------------------\n' +
                        '---PATH: ' + sdf_must_be_path + '\n' +
                        content_got + '\n' +
                        '---END---------------------------------------------------------------------'
                    )
                    self.assertEqual(sdf_must_be, content_got, msg='The sdf was not generated properly!')
                    self.assertEqual(num_items_must_be, total_items,
                                     msg='The total number of items was not returned properly')

    def test_writes_simple_sdf_file_when_no_items_have_structure(self):
        """
        Test it writes simple sdf file when no items have structure
        """
        file_writer = get_basic_sdf_file_writer_where_no_items_have_structure()

        output_dir_path = get_test_output_dir_path()
        base_filename = 'test' + str(int(round(time.time() * 1000)))

        out_file_path, total_items = file_writer.write_sdf_file(output_dir_path, base_filename)

        sdf_must_be_path = os.path.join('download_job/file_writing/test/data/sdf_when_no_structures_must_be.sdf')

        with open(sdf_must_be_path, 'rt') as sdf_file_must_be:
            sdf_must_be = sdf_file_must_be.read()
            num_items_must_be = 0

            with zipfile.ZipFile(out_file_path) as zip_got:
                filename_got = [filename for filename in zip_got.namelist() if not os.path.isdir(filename)][0]

                with zip_got.open(filename_got, 'r') as file_got:
                    content_got = file_got.read().decode()
                    print(
                        '---SDF_IS------------------------------------------------------------------\n' +
                        '---PATH: ' + sdf_must_be_path + '\n' +
                        content_got + '\n' +
                        '---END---------------------------------------------------------------------'
                    )
                    self.assertEqual(sdf_must_be, content_got, msg='The sdf was not generated properly!')
                    self.assertEqual(num_items_must_be, total_items,
                                     msg='The total number of items was not returned properly')

    def test_reports_sdf_file_writing_progress(self):
        """
        Test reports the sdf file writing progress
        """

        file_writer = get_basic_sdf_file_writer()
        output_dir_path = get_test_output_dir_path()
        base_filename = 'test' + str(int(round(time.time() * 1000)))

        progress_got = []

        def progress_function(progress):
            progress_got.append(progress)

        file_writer.write_sdf_file(output_dir_path, base_filename, progress_function=progress_function)

        is_ascending = all(progress_got[i] <= progress_got[i + 1] for i in range(len(progress_got) - 1))
        ends_with100 = progress_got[-1] == 100

        self.assertTrue(is_ascending, msg='The progress is not reported correctly, it should be ascending. '
                                          'I got this: {}\n'.format(str(progress_got)))

        self.assertTrue(ends_with100, msg='The progress must end with 100. I got this: {}\n'.format(
            str(progress_got)))
