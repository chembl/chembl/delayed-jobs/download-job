"""
Module that writes file after collecting data from ES
"""
import os
from enum import Enum
import zipfile
from pathlib import Path
import sys

from elasticsearch.helpers import scan

from download_job.file_writing import es_connection
from download_job.file_writing.utils import dict_property_access
from download_job.file_writing import columns_parser
from download_job.file_writing import file_writer_common


def format_cell(original_value):
    """
    Makes sure the string values are formatted correctly
    :param original_value: raw value
    :return: cell with correct quote scaping
    """
    value = original_value
    if isinstance(value, str):
        value = value.replace('"', "'")

    return f'"{value}"'


def write_header_line(separator, all_columns, out_file):
    """
    Writes the header of the csv or tsv file
    :param separator: separator to use for the values
    :param all_columns: all the columns in the download
    :param out_file: output file to write
    """

    header_line = separator.join([format_cell(col['label']) for col in all_columns])
    out_file.write((header_line + '\n').encode())


def write_item_line(separator, all_values, out_file):
    """
    Writes a line corresponding to an item in the csv or tsv file
    :param separator: separator to use for the values
    :param all_values: a list with the values to write
    :param out_file: output file to write
    :return: the size in lines of the line written
    """

    item_line = separator.join([format_cell(v) for v in all_values])
    out_file.write((item_line + '\n').encode())
    return sys.getsizeof(item_line)


class FileWriter:
    """
    Class that writes files on the initialisation parameters
    """

    # pylint: disable=too-many-instance-attributes

    class OutputFormats(Enum):
        """
        Class that enumerates the output formats available
        """
        CSV = 'CSV'
        TSV = 'TSV'

    class FileWriterError(Exception):
        """Base class for exceptions in the file writer."""

    def __init__(self, desired_format, index_name, query, columns_to_download, context=None,
                 id_property=None, contextual_columns=None, data_index=None):
        """
        Writes a file in a separated values formant (CSV or TSV)
        :param desired_format: desired format of the file
        :param index_name: name of the index to query
        :param query: query to send to ES
        :param columns_to_download: columns to include in the file
        :param context: context object
        :param id_property: property that identifies the items in the index
        :param contextual_columns: columns in the context
        :param data_index: index that contains the items, only if it is different than the original index name. If None
        it will be assumed to be the index name. Used in the search by ids downloads
        """
        # pylint: disable=too-many-arguments

        if desired_format not in list(self.OutputFormats):
            raise self.FileWriterError(f'The format {desired_format} is not supported')

        if index_name is None:
            raise self.FileWriterError('You must provide an index name')

        self.desired_format = desired_format
        self.index_name = index_name
        self.data_index = data_index
        if self.data_index is None:
            self.data_index = index_name
        self.query = query
        self.columns_to_download = columns_to_download
        self.context = context
        self.id_property = id_property
        self.contextual_columns = contextual_columns

        self.using_context = self.review_context()
        self.es_conn = es_connection.get_es_connection_from_env_config()





    def get_search_source(self):
        """
        :return: the list of properties to be used in the query, corresponding to the columns to download
        """
        source = []
        for col in self.columns_to_download:
            prop_name = col.get('prop_id')
            based_on = col.get('based_on')
            if based_on is not None:
                source.append(based_on)
            else:
                source.append(prop_name)

        return source

    def review_context(self):
        """
        Reviews that when providing a context, the id_property and contextual columns are also set
        """
        using_context = False
        if self.context is not None:
            if self.id_property is None:
                raise self.FileWriterError(
                    'When providing context, an id property must be given in order to join the rows')
            if self.contextual_columns is None:
                raise self.FileWriterError('When providing context, an contextual column description must be given')
            using_context = True
        return using_context

    # pylint: disable=inconsistent-return-statements
    def get_separator(self):
        """
        :param desired_format: TSV or CSV
        :return: the corresponding separator
        """

        if self.desired_format is self.OutputFormats.CSV:
            return ';'
        if self.desired_format is self.OutputFormats.TSV:
            return '\t'

    def get_all_columns(self):
        """
        :return: all the columns in the download
        """
        if self.using_context:
            all_columns = self.contextual_columns + self.columns_to_download
        else:
            all_columns = self.columns_to_download

        return all_columns

    def get_scanner(self, source):
        """
        :param source: columns to download from the scanner
        :return: the scanner to get the data
        """
        return scan(
            self.es_conn,
            index=self.data_index,
            scroll=u'1m',
            size=1000,
            request_timeout=60,
            query={
                "_source": source,
                "query": self.query
            }
        )

    def get_total_items(self):
        """
        :return: the total items to download
        """
        return self.es_conn.search(
            index=self.data_index,
            body={'query': self.query, 'track_total_hits': True}
        )['hits']['total']['value']

    def get_own_properties_names_to_get(self):
        """
        :return: the ids of the non contextual properties to get from a document
        """
        own_properties_to_get = []

        for col in self.columns_to_download:
            prop_name = col['prop_id']
            based_on = col.get('based_on')

            own_properties_to_get.append({
                'prop_name': prop_name,
                'based_on': based_on
            })

        return own_properties_to_get

    def get_own_values(self, doc_source):
        """
        :param doc_source: source of the document obtained
        :return: the non contextual values of a document
        """
        own_properties_to_get = self.get_own_properties_names_to_get()

        own_values = []
        for prop_desc in own_properties_to_get:
            prop_name = prop_desc.get('prop_name')
            based_on = prop_desc.get('based_on')
            if based_on is not None:
                prop_to_get = based_on
            else:
                prop_to_get = prop_name
            raw_value = dict_property_access.get_property_value(doc_source, prop_to_get, '')

            parsed_value = columns_parser.parse(raw_value, self.index_name, prop_desc['prop_name'])
            own_values.append(parsed_value)

        return own_values

    def get_contextual_values(self, doc_i):
        """
        :param doc_i: the document for which get the contextual values
        :return: the contextual values corresponding to the document
        """

        contextual_values = []
        if self.using_context:

            doc_id = str(doc_i['_id'])
            context_item = self.context[doc_id]

            if context_item is not None:
                contextual_values = [str(context_item[col['prop_id']]) for col in self.contextual_columns]
            else:
                contextual_values = ['' for i in range(0, len(self.contextual_columns))]

        return contextual_values

    def write_separated_values_file(self, output_dir_path, base_filename,
                                    progress_function=(lambda progress: progress)):
        """
        Writes the file in the path set in the parameter with a filename based on the parameter
        :param output_dir_path: directory where to write the file
        :param base_filename: base name of the file
        :param progress_function: function to be called when as the progress changes
        :return: the path, and the total items included in the path
        """
        separator = self.get_separator()

        Path(output_dir_path).mkdir(parents=True, exist_ok=True)
        output_file_path = os.path.join(output_dir_path, base_filename + '.zip')

        if self.desired_format is self.OutputFormats.CSV:
            extension = 'csv'
        elif self.desired_format is self.OutputFormats.TSV:
            extension = 'tsv'
        else:
            raise self.FileWriterError(f'The format {self.desired_format} is not supported')

        with zipfile.ZipFile(output_file_path, 'w', zipfile.ZIP_DEFLATED, allowZip64=True) as zip_archive:

            # Make a new part every 1 GB inside the zip archive.
            default_partition_size = 1000000000
            partition_size = os.environ.get('MAX_FILE_BYTES', default_partition_size)
            partition_size = default_partition_size if partition_size == '' else partition_size
            partition_size = int(partition_size)

            part_number = 1

            out_file = file_writer_common.get_file_object_for_part(zip_archive, base_filename, part_number, extension)

            all_columns = self.get_all_columns()
            write_header_line(separator, all_columns, out_file)

            source = self.get_search_source()
            print('scanner source: ', source)
            scanner = self.get_scanner(source)

            i = 0
            previous_percentage = 0
            progress_function(previous_percentage)
            total_items = self.get_total_items()
            total_estimated_bytes_written = 0
            estimated_bytes_written = 0

            for doc_i in scanner:
                i += 1
                doc_source = doc_i['_source']

                own_values = self.get_own_values(doc_source)

                contextual_values = self.get_contextual_values(doc_i)
                all_values = contextual_values + own_values

                line_size = write_item_line(separator, all_values, out_file)
                total_estimated_bytes_written += line_size
                estimated_bytes_written += line_size

                if estimated_bytes_written > partition_size:
                    # Switch to a new part
                    part_number += 1
                    estimated_bytes_written = 0
                    out_file.close()
                    out_file = file_writer_common.get_file_object_for_part(zip_archive, base_filename, part_number,
                                                                           extension)

                percentage = int((i / total_items) * 100)
                if percentage != previous_percentage:
                    previous_percentage = percentage
                    progress_function(percentage)

            out_file.close()

        return output_file_path, total_items
