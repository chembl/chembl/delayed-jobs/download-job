#!/usr/bin/env python3
"""
Script that runs a job that creates files with subsets of chembl data to be downloaded from the interface
"""
import argparse
import os
import time

import yaml

from download_job.server_connection import DelayedJobsServerConnection
from download_job import file_creator

PARSER = argparse.ArgumentParser()
PARSER.add_argument('run_params_file', help='The path of the file with the run params')
PARSER.add_argument('-v', '--verbose', help='Make output verbose', action='store_true')
PARSER.add_argument('-n', '--dry-run', help='Do not actually send progress to the delayed jobs server',
                    action='store_true')
ARGS = PARSER.parse_args()


def run():
    """
    Runs the job
    """
    initial_time = time.time()

    run_params = yaml.load(open(ARGS.run_params_file, 'r'), Loader=yaml.FullLoader)
    job_params = run_params.get('job_params')

    download_params = job_params
    server_conn = DelayedJobsServerConnection(run_params_file_path=ARGS.run_params_file, dry_run=ARGS.dry_run)

    custom_config = run_params.get('custom_job_config')
    print('custom_config: ', custom_config)
    if custom_config is not None:
        load_custom_config_as_env_variables(custom_config)

    job_id = run_params.get('job_id')
    print('job_id: ', job_id)
    output_dir = run_params.get('output_dir')
    print('output_dir: ', output_dir)

    server_conn.update_job_progress(0, 'Going to load data from elasticsearch.')
    output_file_path, stats_dict = file_creator.generate_download_file(download_params, server_conn, output_dir, job_id)
    server_conn.update_job_progress(100, 'File created!')

    end_time = time.time()
    stats_dict = {
        'time_taken': end_time - initial_time,
        **stats_dict
    }

    print('output_file_path: ', output_file_path)
    server_conn.send_job_statistics(stats_dict)


def load_custom_config_as_env_variables(custom_config):
    """
    Loads the custom configuration dict into the env variables
    :param custom_config: the custom configuration for the job
    """
    for key, value in custom_config.items():
        os.environ[key.upper()] = str(value)


if __name__ == "__main__":
    run()
