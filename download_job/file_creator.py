"""
Module that creates the download files
"""
import os
import json
import re

import requests

from download_job.file_writing.file_writer import FileWriter
from download_job.file_writing.sdf_file_writer import SDFFileWriter
from download_job.file_writing.utils import dict_property_access


class FileCreatorError(Exception):
    """
    Class for exceptions in this module
    """


def generate_download_file(download_params, server_conn, output_dir, job_id):
    """
    Generates a download file with the parameters given
    :param download_params: parameters for creating the download file
    :param server_conn: object to handle the connection with the server
    :param output_dir: output directory where to save the file
    :param job_id: id of the job to use it as base filename
    :return: the path where the file was created
    """
    desired_format = download_params['format'].upper()

    if desired_format in ['CSV', 'TSV']:
        out_file_path, stats_dict = generate_separated_values_file(download_params, output_dir, job_id, server_conn)
    elif desired_format == 'SDF':
        out_file_path, stats_dict = generate_sdf_file(download_params, output_dir, job_id, server_conn)
    else:
        raise FileCreatorError(f'The format {desired_format} is not available')

    stats_dict = {
        'desired_format': desired_format,
        **stats_dict
    }
    return out_file_path, stats_dict


def generate_separated_values_file(download_params, output_dir, job_id, server_conn):
    """
    Generates a separated values file with the parameters passed
    :param download_params: the dict with the parameters for the download
    :param output_dir: output directory where to save the file
    :param job_id: id of the job to use it as base filename
    :param server_conn: conextion with the server
    :return: the output file path and the statistics object
    """

    desired_format = download_params['format'].upper()
    if desired_format == 'CSV':
        standardised_format = FileWriter.OutputFormats.CSV

    if desired_format == 'TSV':
        standardised_format = FileWriter.OutputFormats.TSV

    index_name = download_params['index_name']
    data_index = None
    query = json.loads(download_params['query'])

    download_columns_group = download_params.get('download_columns_group', 'download')
    non_contextual_columns, contextual_columns = get_columns_to_download(index_name, download_columns_group)
    print('contextual_columns: ', contextual_columns)

    context_obj = download_params.get('context_obj')
    search_by_ids_context = is_search_by_ids_context(context_obj)

    if context_obj is None:
        id_property = None
        context = None
    elif search_by_ids_context:
        id_property = None
        context = None  # context could be loaded with matches data as a next iteration
        data_index = load_index_name_for_search_by_ids(context_obj)
    else:
        # IT DOES NOT SUPPORT MULTIPLE PROPERTIES YET, IT WILL USE THE FIRST PROPERTY IN THE LIST
        id_properties = get_id_properties(index_name)
        id_property = id_properties[0]

        server_conn.update_job_progress(0, 'Going to load context.')
        raw_context = load_context(context_obj)
        context = load_context_index(id_properties, raw_context)
        server_conn.update_job_progress(0, 'Context loaded!')
        print('context index')
        print(json.dumps(context, indent=4))

        add_ids_from_context_to_query(query, context, id_property)

    print('query: ', query)

    file_writer = FileWriter(
        desired_format=standardised_format,
        index_name=index_name,
        query=query,
        columns_to_download=non_contextual_columns,
        contextual_columns=contextual_columns,
        id_property=id_property,
        context=context,
        data_index=data_index
    )

    progress_function = create_progress_function(server_conn)
    server_conn.update_job_progress(0, 'Going to start to write file!')
    out_file_path, total_items = file_writer.write_separated_values_file(output_dir, job_id, progress_function)
    server_conn.update_job_progress(100, 'File written!')

    stats_dict = {
        'file_size': os.path.getsize(out_file_path),
        'es_index': index_name,
        'es_query': download_params['query'],
        'total_items': total_items
    }
    return out_file_path, stats_dict


def is_search_by_ids_context(context_obj):
    """
    :param context_obj: context dict with the context description
    :return: True if it is search by ids, False otherwise
    """
    if context_obj is None:
        return False

    context = json.loads(context_obj)
    context_type = context['context_type']
    return context_type in ['TO_COMPOUNDS', 'TO_TARGETS']


def is_direct_structure_search_context(context_obj):
    """
    :param context_obj: context string with the context description
    :return: True if it is direct structure search context, False otherwise
    """
    if context_obj is None:
        return False

    context = json.loads(context_obj)
    context_type = context['context_type']

    return context_type == 'DIRECT_SIMILARITY'


def load_index_name_for_search_by_ids(context_str):
    """
    loads the index name for the search by ids results
    :param context_str: context dict with the context description
    :return: the index name to use for getting the items
    """
    context = json.loads(context_str)
    print('Going to load index name from context: ')
    print(json.dumps(context, indent=4))

    delayed_jobs_base_url = context['delayed_jobs_base_url']
    context_id = context['context_id']
    status_url = f'{delayed_jobs_base_url}/status/{context_id}'
    status = requests.get(status_url).json()
    summary_url = revise_url_scheme(status['output_files_urls']['summary.json'])
    print('summary_url: ', summary_url)
    summary = requests.get(summary_url).json()
    print('summary: ')
    print(json.dumps(summary, indent=4))
    index_name = summary['subset_index_name']
    print('index_name: ', index_name)

    return index_name


def add_ids_from_context_to_query(query, context, id_property):
    """
    Adds the ids from the context to the query, in some cases it is necessary,
    fox example, in the direct similarity search
    :param query: query to use for the download
    :param context: context loaded
    :param id_property: property that identifies the items
    """
    all_ids = [dict_property_access.get_property_value(result, id_property) for result in context.values()]

    if query['bool'] is None:
        query['bool'] = {}

    if query['bool']['must'] is None:
        query['bool']['must'] = []

    terms_query = {
        "terms": {
            f'{id_property}': all_ids
        }
    }

    query['bool']['must'].append(terms_query)


def revise_url_scheme(original_url):
    """
    :param original_url:
    :return: the url making sure that it has the correct scheme
    """
    print('revise_url_scheme: ')
    print(original_url)
    # Make sure to always use http because connection is internal
    scheme_match = re.search(r'^http(s)?://', original_url)
    if scheme_match is None:
        return f'http://{original_url}'

    scheme = scheme_match.group(0)
    if scheme is None:
        return f'http://{original_url}'
    return original_url.replace(scheme, 'http://')


def get_columns_to_download(index_name, download_colums_group):
    """
    :param index_name: name of the index for which to query the columns
    :param download_colums_group: group name of columns to download
    :return: the description of the columns to be used in the download
    """
    es_proxy_base_url = os.environ.get('es_proxy_base_url'.upper())

    if es_proxy_base_url is None:
        raise FileCreatorError('The es_proxy_base_url is not set!')

    columns_to_download_url = f'{es_proxy_base_url}/properties_configuration/group/{index_name}/{download_colums_group}'
    print('columns_to_download_url: ', columns_to_download_url)
    config_request = requests.get(columns_to_download_url)
    response_json = config_request.json()
    print('response_json: ', response_json)
    columns_to_download = response_json.get('properties').get('default')

    contextual_columns = [col for col in columns_to_download if col.get('is_contextual', False)]
    non_contextual_columns = [col for col in columns_to_download if not col.get('is_contextual', False)]

    return non_contextual_columns, contextual_columns


def generate_sdf_file(download_params, output_dir, job_id, server_conn):
    """
    Generates a sdf file with the parameters passed
    :param download_params: the dict with the parameters for the download
    :param output_dir: output directory where to save the file
    :param job_id: id of the job to use it as base filename
    :param server_conn: conextion with the server
    :return: the output file path and the statistics object
    """
    query = json.loads(download_params['query'])
    context_obj = download_params.get('context_obj')
    search_by_ids_context = is_search_by_ids_context(context_obj)
    direct_structure_search_context = is_direct_structure_search_context(context_obj)

    data_index = None
    if search_by_ids_context:
        data_index = load_index_name_for_search_by_ids(context_obj)

    if direct_structure_search_context:
        index_name = download_params['index_name']
        id_properties = get_id_properties(index_name)
        id_property = id_properties[0]
        raw_context = load_context(context_obj)
        context = load_context_index(id_properties, raw_context)
        add_ids_from_context_to_query(query, context, id_property)

    file_writer = SDFFileWriter(query=query, data_index=data_index)

    progress_function = create_progress_function(server_conn)
    out_file_path, total_items = file_writer.write_sdf_file(output_dir, job_id, progress_function)

    stats_dict = {
        'file_size': os.path.getsize(out_file_path),
        'es_index': 'chembl_molecule',
        'es_query': download_params['query'],
        'total_items': total_items
    }
    return out_file_path, stats_dict


def get_id_properties(index_name):
    """
    :param index_name: index for which to get the id properties.
    :return: the id properties of the index given as parameter
    """
    es_proxy_base_url = os.environ.get('es_proxy_base_url'.upper())

    if es_proxy_base_url is None:
        raise FileCreatorError('The es_proxy_base_url is not set!')

    id_properties_url = f'{es_proxy_base_url}/properties_configuration/id_properties/{index_name}'

    id_properties_request = requests.get(id_properties_url)
    response_json = id_properties_request.json()
    id_properties = response_json.get('id_properties')

    return id_properties


def load_context(context_obj):
    """
    :param context_obj: object describing the context
    :return: the context described by the object passed as parameter
    """
    es_proxy_base_url = os.environ.get('es_proxy_base_url'.upper())
    if es_proxy_base_url is None:
        raise FileCreatorError('The es_proxy_base_url is not set!')

    payload = {
        "context_obj": json.dumps(context_obj)
    }

    context_data_url = f'{es_proxy_base_url}/contexts/get_context_data'
    print('context_data_url: ', context_data_url)
    print('payload: ', payload)

    request = requests.post(context_data_url, data=payload)
    request_status_code = request.status_code
    assert request_status_code == 200, f'The context status code was {request_status_code}'
    response_json = request.json()

    raw_context = response_json.get('context_data')
    return raw_context


def load_context_index(id_properties_list, context):
    """
    Loads an index based on the id property of the context, for fast access
    :param id_properties_list: property used to identify each item
    :param context: context loaded
    :return:
    """
    context_index = {}
    for index_number, item in enumerate(context):
        id_value = get_id_value(id_properties_list, item)
        context_index[id_value] = item
        context_index[id_value]['index'] = index_number

    return context_index


def get_id_value(id_properties_list, item: dict):
    """
    :param id_properties_list: the properties that identify the item
    :param item: the item for which to get the id value
    :return: the value that identifies the item
    """
    id_values = [str(item.get(id_property)) for id_property in id_properties_list]

    return '-'.join(id_values)


def create_progress_function(server_conn):
    """
    :param server_conn: object that represents the connection to the server
    :return: a function to report the progress to the delayed job system
    """

    def report_progress(progress):
        """
        Reports the progress to the delayed jobs server
        :param progress: progress percentage of the job
        :return: the function to be used for reporting the progress
        """
        server_conn.update_job_progress(progress, f'Processed {progress}% of the items.')

    return report_progress
